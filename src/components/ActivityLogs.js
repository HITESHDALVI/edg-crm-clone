import React from "react";
import "./ActivityLogs.css";
import Box from "@mui/material/Box";
// import manImg from "../images/manImg.jpg";
import AssignmentOutlinedIcon from "@mui/icons-material/AssignmentOutlined";

export function ActivityLogs() {
  const [value, setValue] = React.useState("1");
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div className="navtabsection">
      <div>
        <Box sx={{ width: "100%", typography: "body1" }}>
          <div className="navcard">
            <AssignmentOutlinedIcon
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                border: "1px solid orange",
                padding: "7px",
                borderRadius: "50%",
                color: "orange",
              }}
            />
            <div>
              <div class="card navcard-resize">
                <div class="card-body navcard-body">
                  <div
                    style={{ paddingLeft: "14px", paddingRight: "14px" }}
                    className="navcard-leftsection"
                  >
                    <a>
                      <p className="LogoNav-resize">AWI</p>
                    </a>
                  </div>
                  <div className="navcard-rightsection">
                    <p className="heading-nav">
                      A Wanda Maximoff<span>02-May-2022 12:53</span>
                    </p>
                    <p className="nav-subtitile">
                      A Wanda Maximoff has updated the call : test
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="navcard">
            <AssignmentOutlinedIcon
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                border: "1px solid orange",
                padding: "7px",
                borderRadius: "50%",
                color: "orange",
              }}
            />
            <div>
              <div class="card navcard-resize">
                <div class="card-body navcard-body">
                  <div className="navcard-leftsection">
                    <img
                      style={{
                        width: "36px",
                        height: "36px",
                        borderRadius: "50%",
                        padding: "14px",
                      }}
                      alt="Edge Consultant"
                      src="https://testing.edgecrm.in/files/1c3/83c/d30/b7c/298/ab5/029/3ad/fec/b7b/1c383cd30b7c298ab50293adfecb7b18.jpg"
                      class="jss6"
                    />
                  </div>
                  <div className="navcard-rightsection">
                    <p className="heading-nav">
                      Edge Consultant <span>04-May-2022 01:12</span>
                    </p>
                    <p className="nav-subtitile">
                      Edge Consultant has updated the meeting : adhgjgjgcgj gjg
                      jg jg j g j gj gj gjg jg j
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="navcard">
            <AssignmentOutlinedIcon
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                border: "1px solid orange",
                padding: "7px",
                borderRadius: "50%",
                color: "orange",
              }}
            />
            <div>
              <div class="card navcard-resize">
                <div class="card-body navcard-body">
                  <div className="navcard-leftsection">
                    <img
                      style={{
                        width: "36px",
                        height: "36px",
                        borderRadius: "50%",
                        padding: "14px",
                      }}
                      alt="Edge Consultant"
                      src="https://testing.edgecrm.in/files/1c3/83c/d30/b7c/298/ab5/029/3ad/fec/b7b/1c383cd30b7c298ab50293adfecb7b18.jpg"
                      class="jss6"
                    />
                  </div>
                  <div className="navcard-rightsection">
                    <p className="heading-nav">
                      Edge Consultant <span>04-May-2022 01:12</span>
                    </p>
                    <p className="nav-subtitile">
                      Edge Consultant has updated the meeting : adhgjgjgcgj gjg
                      jg jg j g j gj gj gjg jg j
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="navcard">
            <AssignmentOutlinedIcon
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                border: "1px solid orange",
                padding: "7px",
                borderRadius: "50%",
                color: "orange",
              }}
            />
            <div>
              <div class="card navcard-resize">
                <div class="card-body navcard-body">
                  <div className="navcard-leftsection">
                    <img
                      style={{
                        width: "36px",
                        height: "36px",
                        borderRadius: "50%",
                        padding: "14px",
                      }}
                      alt="Edge Consultant"
                      src="https://testing.edgecrm.in/files/1c3/83c/d30/b7c/298/ab5/029/3ad/fec/b7b/1c383cd30b7c298ab50293adfecb7b18.jpg"
                      class="jss6"
                    />
                  </div>
                  <div className="navcard-rightsection">
                    <p className="heading-nav">
                      Edge Consultant <span>04-May-2022 01:12</span>
                    </p>
                    <p className="nav-subtitile">
                      Edge Consultant has updated the meeting : adhgjgjgcgj gjg
                      jg jg j g j gj gj gjg jg j
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="navcard">
            <AssignmentOutlinedIcon
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                border: "1px solid orange",
                padding: "7px",
                borderRadius: "50%",
                color: "orange",
              }}
            />
            <div>
              <div class="card navcard-resize">
                <div class="card-body navcard-body">
                  <div className="navcard-leftsection">
                    <img
                      style={{
                        width: "36px",
                        height: "36px",
                        borderRadius: "50%",
                        padding: "14px",
                      }}
                      alt="Edge Consultant"
                      src="https://testing.edgecrm.in/files/1c3/83c/d30/b7c/298/ab5/029/3ad/fec/b7b/1c383cd30b7c298ab50293adfecb7b18.jpg"
                      class="jss6"
                    />
                  </div>
                  <div className="navcard-rightsection">
                    <p className="heading-nav">
                      Edge Consultant <span>04-May-2022 01:12</span>
                    </p>
                    <p className="nav-subtitile">
                      Edge Consultant has updated the meeting : adhgjgjgcgj gjg
                      jg jg j g j gj gj gjg jg j
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <button
            variant="text"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              fontSize: "14px",
              color: "rgb(11, 59, 144",
              cursor: "pointer",
              fontFamily: '"Nunito Sans", sans-serif',
              border: "none",
              outline: "none",
              marginLeft: "50%",
              marginTop: "50px",
              marginBottom: "50px",
            }}
            type="link"
          >
            Show More
          </button>
        </Box>
      </div>
    </div>
  );
}
