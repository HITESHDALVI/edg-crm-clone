import React from "react";
import "./SideMenu.css";
import manImg from "../images/manImg.jpg";
import DashboardOutlinedIcon from "@mui/icons-material/DashboardOutlined";
import DateRangeOutlinedIcon from "@mui/icons-material/DateRangeOutlined";
import CardTravelOutlinedIcon from "@mui/icons-material/CardTravelOutlined";
import PersonPinOutlinedIcon from "@mui/icons-material/PersonPinOutlined";
import LocalLibraryOutlinedIcon from "@mui/icons-material/LocalLibraryOutlined";
import CampaignOutlinedIcon from "@mui/icons-material/CampaignOutlined";
import SupportAgentOutlinedIcon from "@mui/icons-material/SupportAgentOutlined";
import GavelOutlinedIcon from "@mui/icons-material/GavelOutlined";
import TungstenOutlinedIcon from "@mui/icons-material/TungstenOutlined";
import InsightsSharpIcon from "@mui/icons-material/InsightsSharp";
export const SideMenu = () => {
  return (
    <ul className="side-menu-list">
      <li className="sidemenu-items">
        <img
          style={{ width: "30px", height: "30px", borderRadius: "50%" }}
          src={manImg}
          alt="manImg"
        />
      </li>
      <li className="sidemenu-items">
        <DashboardOutlinedIcon />
      </li>
      <li className="sidemenu-items">
        <DateRangeOutlinedIcon />
      </li>
      <li className="sidemenu-items">
        <CardTravelOutlinedIcon />
      </li>
      <li className="sidemenu-items">
        <PersonPinOutlinedIcon />
      </li>
      <li className="sidemenu-items">
        <TungstenOutlinedIcon />
      </li>
      <li className="sidemenu-items">
        <LocalLibraryOutlinedIcon />
      </li>
      <li className="sidemenu-items">
        <CampaignOutlinedIcon />
      </li>
      <li className="sidemenu-items">
        <SupportAgentOutlinedIcon />
      </li>
      <li className="sidemenu-items">
        <GavelOutlinedIcon />
      </li>
      <li className="sidemenu-items">
        <InsightsSharpIcon />
      </li>
    </ul>
  );
};
