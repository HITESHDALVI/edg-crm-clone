import React from "react";
import "./Accordian.css";
import ArrowForwardIosOutlinedIcon from "@mui/icons-material/ArrowForwardIosOutlined";
import "bootstrap";
import KeyboardArrowDownOutlinedIcon from "@mui/icons-material/KeyboardArrowDownOutlined";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import Input from "@mui/material/Input";
import FormControl from "@mui/material/FormControl";

export const Accordian = () => {
  return (
    <div className="accordian ">
      <div className="accordian-1-item">
        <div className="acc-1">
          <div className="accordian-name">Contact information</div>
          <KeyboardArrowDownOutlinedIcon
            style={{ fontSize: "29px", fontWeight: "700" }}
          />
        </div>
        <div style={{ boxSizing: "border-box" }}>
          <FormControl
            fullWidth
            sx={{ m: 1, width: "350px" }}
            variant="standard"
          >
            <InputLabel
              style={{ fontSize: "16px", paddingLeft: "17px" }}
              htmlFor="standard-adornment-amount"
            >
              Contact Number 1
            </InputLabel>
            <Input
              disableUnderline
              placeholder="Phone No"
              style={{
                fontSize: "15px",
              }}
              sx={{
                m: 1,
              }}
              id="standard-adornment-amount"
              startAdornment={
                <InputAdornment position="start"></InputAdornment>
              }
            />
          </FormControl>
          <FormControl
            fullWidth
            sx={{ m: 1, width: "350px" }}
            variant="standard"
          >
            <InputLabel
              style={{ fontSize: "16px", paddingLeft: "17px" }}
              htmlFor="standard-adornment-amount"
            >
              Contact Number 2
            </InputLabel>
            <Input
              disableUnderline
              placeholder="Phone No"
              style={{ fontSize: "15px" }}
              sx={{
                m: 1,
              }}
              id="standard-adornment-amount"
              startAdornment={
                <InputAdornment position="start"></InputAdornment>
              }
            />
          </FormControl>
          <FormControl
            fullWidth
            sx={{ m: 1, width: "350px" }}
            variant="standard"
          >
            <InputLabel
              style={{ fontSize: "16px", paddingLeft: "17px" }}
              htmlFor="standard-adornment-amount"
            >
              Email Id
            </InputLabel>
            <Input
              disableUnderline
              placeholder="nilesgmhase07.tg+1319@gmail.com"
              style={{ fontSize: "15px" }}
              sx={{
                m: 1,
              }}
              id="standard-adornment-amount"
              startAdornment={
                <InputAdornment position="start"></InputAdornment>
              }
            />
          </FormControl>
        </div>
      </div>
      <div className="accordian-item">
        <div className="accordian-name">About Peter Petrino</div>
        <ArrowForwardIosOutlinedIcon
          style={{ fontSize: "17px", fontWeight: "700" }}
        />
      </div>
      <div className="accordian-item">
        <div className="accordian-name"> Contact Address</div>
        <ArrowForwardIosOutlinedIcon
          style={{ fontSize: "17px", fontWeight: "700" }}
        />
      </div>
      <div className="accordian-item">
        <div className="accordian-name">Peter Petrino Leads(0)</div>
        <ArrowForwardIosOutlinedIcon
          style={{ fontSize: "17px", fontWeight: "700" }}
        />
      </div>
      <div className="accordian-item">
        <div className="accordian-name"> Files</div>
        <ArrowForwardIosOutlinedIcon
          style={{ fontSize: "17px", fontWeight: "700" }}
        />
      </div>
    </div>
  );
};
