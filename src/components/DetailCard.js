import React from "react";
// import CardActions from "@mui/material/CardActions";
import "./ActivityArea.css";

export const DetailCard = (props) => {
  return (
    <>
      <div
        key={props.key}
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          boxSizing: "border-box",
          fontFamily: '"Nunito Sans", sans-serif',
          width: "750px",
          height: "85px",
        }}
      >
        <img
          style={{
            width: "40px",
            height: "40px",
            borderRadius: "50%",
            marginLeft: "7px",
            padding: "5px",
          }}
          alt="Edge Consultant"
          src="https://testing.edgecrm.in/files/1c3/83c/d30/b7c/298/ab5/029/3ad/fec/b7b/1c383cd30b7c298ab50293adfecb7b18.jpg"
          class="jss6"
        />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            padding: "10px",
            width: "auto",
            height: "80px",
            boxSizing: "border-box",
          }}
        >
          <h5
            style={{
              fontFamily: '"Nunito Sans", sans-serif',
              marginBlockStart: "0",
              marginBlockEnd: "0",
              fontSize: "18px",
              fontWeight: "300",
              color: " rgb(0, 29, 82)",
            }}
          >
            Edge Consultant
          </h5>
          <p
            style={{
              display: "flex",
              flexDirection: "row",
              marginBlockStart: "0",
              marginBlockEnd: "0",
              boxSizing: "border-box",
              fontSize: "13px",
              fontWeight: "300",
              color: "rgb(122, 122, 122",
            }}
          >
            created a task
            <span
              style={{
                color: "#ff8000",
                fontFamily: '"Nunito Sans", sans-serif',
              }}
            >
              {props.value}
            </span>
            dated Apr 21, 2022 .
          </p>
        </div>
      </div>
    </>
  );
};
