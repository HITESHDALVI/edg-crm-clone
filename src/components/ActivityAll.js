import React from "react";
import { Stack } from "@mui/material";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
// import CardActions from "@mui/material/CardActions";
import "./ActivityArea.css";
import PhoneInTalkIcon from "@mui/icons-material/PhoneInTalk";
import { DetailCard } from "./DetailCard";

export const ActivityAll = () => {
  const elements = [
    "'Call to check RESULT'",
    "'hdhhh hj h hj hj hj hj hkhjkhk jhkhkhkh hkhkhkhk kjh kh kh kh hjk hkh khkhk' ",
    "'JJASDJLAJSLDALSJL JLJL JL JLJL JL L JL J'",
    " 'UDUSAOUOUOUO UOU OUO UO U OUO UO UO'",
    " 'adhgjgjgcgj gjg jg jg j g j gj gj gjg jg j'",
  ];

  return elements.map((value, index) => {
    if (index !== 1) {
      return (
        <div className="activity-call">
          <PhoneInTalkIcon
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              border: "1px solid orange",
              padding: "7px",
              borderRadius: "50%",
              color: "orange",
            }}
          />
          <div>
            <Box
              style={{
                boxSizing: "border-box",
                Width: "750px",
                height: "90px",
                background: "white",
                boxShadow: "0px 4px 18px rgb(189 37 37 / 6%)",
              }}
            >
              <Card variant="outlined">
                <DetailCard key={index} value={value} />
              </Card>
            </Box>

            <Stack spacing={2} direction="row">
              <button variant="text" className="btn-call" type="link">
                View
              </button>
              <button variant="text" className="btn-call" type="link">
                Edit
              </button>
              <button
                type="link"
                variant="text"
                style={{ color: "red" }}
                className="btn-call"
              >
                Delete
              </button>
            </Stack>
          </div>
        </div>
      );
    } else {
      return (
        <div className="activity-call">
          <PhoneInTalkIcon
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              border: "1px solid orange",
              padding: "7px",
              borderRadius: "50%",
              color: "orange",
            }}
          />
          <div>
            <Box
              style={{
                boxSizing: "border-box",
                Width: "750px",
                height: "90px",
                background: "white",
                boxShadow: "0px 4px 18px rgb(189 37 37 / 6%)",
              }}
            >
              <Card variant="outlined">
                <DetailCard key={index} value={value} />
              </Card>
            </Box>

            <Stack spacing={2} direction="row">
              <button variant="text" className="btn-call" type="link">
                View
              </button>
              <button variant="text" className="btn-call" type="link">
                Edit
              </button>
              <button
                type="link"
                variant="text"
                style={{ color: "red" }}
                className="btn-call"
              >
                Delete
              </button>
              <button variant="text" className="btn-call" type="link">
                Mark as Complete
              </button>
            </Stack>
          </div>
        </div>
      );
    }
  });
};
