import React from "react";
// import { button } from "antd";
import { Stack } from "@mui/material";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import "./ActivityArea.css";
import GroupsIcon from "@mui/icons-material/Groups";
const card = (
  <React.Fragment>
    <CardActions
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        boxSizing: "border-box",
        fontFamily: '"Nunito Sans", sans-serif',
        width: "750px",
        height: "85px",
      }}
    >
      <img
        style={{
          width: "40px",
          height: "40px",
          borderRadius: "50%",
          padding: "5px",
        }}
        alt="Edge Consultant"
        src="https://testing.edgecrm.in/files/1c3/83c/d30/b7c/298/ab5/029/3ad/fec/b7b/1c383cd30b7c298ab50293adfecb7b18.jpg"
        class="jss6"
      />
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          //   justifyContent: "center",
          padding: "10px",
          width: "auto",
          height: "80px",
          boxSizing: "border-box",
        }}
      >
        <h5
          style={{
            fontFamily: '"Nunito Sans", sans-serif',
            marginBlockStart: "0",
            marginBlockEnd: "0",
            fontSize: "18px",
            fontWeight: "300",
            color: " rgb(0, 29, 82)",
          }}
        >
          Edge Consultant
        </h5>
        <p
          style={{
            display: "flex",
            flexDirection: "row",
            marginBlockStart: "0",
            marginBlockEnd: "0",
            boxSizing: "border-box",
            fontSize: "13px",
            fontWeight: "300",
            color: "rgb(122, 122, 122",
          }}
        >
          created a task
          <span
            style={{
              color: "#ff8000",
              fontFamily: '"Nunito Sans", sans-serif',
            }}
          >
            'adhgjgjgcgj gjg jg jg j g j gj gj gjg jg j'
          </span>
          dated Apr 21, 2022 .
        </p>
      </div>
    </CardActions>
  </React.Fragment>
);

export const ActivityMeet = () => {
  return (
    <div className="activity-call">
      <GroupsIcon
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          border: "1px solid orange",
          padding: "7px",
          borderRadius: "50%",
          color: "orange",
        }}
      />
      <div>
        <Box
          style={{
            boxSizing: "border-box",
            Width: "750px",
            height: "90px",
            background: "white",
            boxShadow: "0px 4px 18px rgb(189 37 37 / 6%)",
          }}
        >
          <Card variant="outlined">{card}</Card>
        </Box>

        <Stack spacing={2} direction="row">
          <button variant="text" className="btn-call" type="link">
            View
          </button>
          <button variant="text" className="btn-call" type="link">
            Edit
          </button>
          <button
            type="link"
            variant="text"
            style={{ color: "red" }}
            className="btn-call"
          >
            Delete
          </button>
        </Stack>
      </div>
    </div>
  );
};
