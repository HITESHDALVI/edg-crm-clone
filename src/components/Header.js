import React from "react";
import "./Header.css";
import KeyboardBackspaceSharpIcon from "@mui/icons-material/KeyboardBackspaceSharp";

export const Header = () => {
  return (
    <div
      style={{ paddingRight: "15px", paddingLeft: "15px" }}
      className="main-header"
    >
      <div style={{ width: "40vw" }}>
        <div
          style={{ display: "flex", fontSize: "13px" }}
          className="repository-back"
        >
          <KeyboardBackspaceSharpIcon
            style={{ fontSize: "19px", fontWeight: "500" }}
          />
          Repository List
        </div>
        <h3
          style={{
            fontSize: "24px",
            color: "#333",
            fontWeight: "300",
            margin: "0",

            padding: "0",
            marginBlockStart: "0",
            marginBlockEnd: "0",
            marginTop: "7px",
          }}
        >
          Peter Petrino
        </h3>
        <p style={{ margin: "0 0 10px" }}>Bigham Jewelers </p>
      </div>
      <div
        style={{
          textAlign: "right",
          display: "flex",
          flexDirection: "row",
          width: "auto",
          height: "15px",
        }}
        className="repository-back"
      >
        <div style={{ marginRight: "10px", fontSize: "14px" }}>Create Lead</div>
        <div style={{ marginRight: "10px", fontSize: "14px" }}>Next</div>
      </div>
    </div>
  );
};
